﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlyCoordinationSystem.Model
{
    // Samolot
    //Nazwa_Samolotu(identyfikator)
    //Waga samolotu
    //Aktualna prędkość(z którą zazwyczaj będzie latać samolot…)
    //Przyspieszenie samolotu(od zera do Vmax)
    //Maksymalna prędkość(na podstawie wagi samolotu)
    //Maksymalna siła wiatru(przy której samolot może lądować) (na podstawie wagi samolotu)
    //Spalanie- wyliczane property(na podstawie wagi samolotu i jego prędkości, ewentualnie kierunku i sile wiatru)
    //PYTANIE - czy samolot powinien mieć dane o wietrze jaki aktualnie wokół niego jest? Chyba tak…
    //Jeżeli tak to: kierunek wiatru
    //Jeżeli tak to: siła wiatru
    //SpalaniePrzyPrzyspieszeniu- ilość o jaką zwiększa się spalanie, gdy samolot przyspiesza(skalar dodawany do zmiennej Spalania)
    //Metoda: potrzebna zmiana terminu lądowania, po negocjacjach, metoda zwraca true lub false (wywoluje kontroler)
    //Metody: metoda negocjacje(tyle metod ile funkcjonalności w obydwie strony: pomiędzy samolotem, a kontrolerem lotów)
    //Picture box(+ metoda do rysowania)- wywoływana za każdym razem, gdy zmienią się dane, np.o prędkośći, stanie paliwa, itd..
    //Ilość paliwa
    /// <summary>
    /// 
    /// </summary>
   public class Samolot
    {
       public string nazwa { get; set; }
       public decimal waga { get; set; }
        /// <summary>
        /// wyrażana w  X/h lub Y/h (wartosci dodawane lub odejmowane od X,Y zależą od kierunku Samolotu)
        /// </summary>
       public int aktualnaPredkosc { get; set; }
        /// <summary>
        /// Kwant czasu potrzebny na osiągnięcie maksymalnej prędkości (od zera do maks).
        /// </summary>
       public int przyspieszenie { get; set; }
        /// <summary>
        /// Maksymalna prędkość X/h lub Y/h jaką może osiągnąć samolot 
        /// </summary>
       public int maksymalnaPredkosc { get; set; }
        /// <summary>
        /// Maksymalna siła wiatru, przy której samolot może lądować.
        /// </summary>
       public int maksymalnaSilaWiatru { get; set; }
        /// <summary>
        /// Ilość o jaką zmniejsza się ilość paliwa, co dany kwant czasu.
        /// </summary>
       public decimal spalanie { get; set; }
        /// <summary>
        /// Aktualny kierunek wiatru
        /// </summary>
       public Kierunek kierunekWiatru { get; set; }
        /// <summary>
        /// Kierunek lotu samolotu
        /// </summary>
       public Kierunek kierunekSamolotu { get; set; }
        /// <summary>
        /// AKtualna prędkość danego kierunku wiatru.
        /// </summary>
       public int predkoscWiatru { get; set; }
        /// <summary>
        ///  ilość o jaką zwiększa się spalanie, gdy samolot przyspiesza(skalar dodawany do zmiennej Spalania)
        /// </summary>
        public decimal SpalaniePrzyPrzyspieszeniuLubZwalnianiu { get; set; }
        //Picture box(+ metoda do rysowania)- wywoływana za każdym razem, gdy zmienią się dane, np.o prędkośći, stanie paliwa, itd..
       public FCSPictureBox grafika { get; set; }
       private int X { get; set; }
       private int Y { get; set; }
        public Samolot()
        {
            grafika = new FCSPictureBox(this);
        }
     public Samolot(string nazwa, decimal waga, int aktualnaPredkosc, int przyspieszenie, int maksymalnaPredkosc, int maksymalnaSilaWiatru, decimal spalanie, Kierunek kierunekWiatru, int predkoscWiatru, decimal SpalaniePrzyPrzyspieszeniuLubZwalnianiu)
        {
            this.nazwa = nazwa;
            this.waga = waga;
            this.aktualnaPredkosc = aktualnaPredkosc;
            this.przyspieszenie = przyspieszenie;
            this.maksymalnaPredkosc = maksymalnaPredkosc;
            this.maksymalnaSilaWiatru = maksymalnaSilaWiatru;
            this.spalanie = spalanie;
            this.kierunekWiatru = kierunekWiatru;
            this.predkoscWiatru = predkoscWiatru;
            this.SpalaniePrzyPrzyspieszeniuLubZwalnianiu = SpalaniePrzyPrzyspieszeniuLubZwalnianiu;
            grafika = new FCSPictureBox(this);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Funkcja zwraca X i Y w formacie: ( X;Y )</returns>
        public string PobierzXY()
        {
            return "( " + X.ToString() + ";" + Y.ToString() + " )";
        }

    }
}
