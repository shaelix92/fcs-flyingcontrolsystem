﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyCoordinationSystem.Model
{
   public class MatkaNatura
    {
        public MatkaNatura( int predkoscWiatru, Kierunek kierunekWiatru)
        {
            this.predkoscWiatru = predkoscWiatru;
            this.kierunekWiatru = kierunekWiatru;
        }
        /// <summary>
        /// Aktualny kierunek wiatru
        /// </summary>
        private Kierunek kierunekWiatru { get; set; }
        /// <summary>
        /// AKtualna prędkość danego kierunku wiatru.
        /// </summary>
        private int predkoscWiatru { get; set; }
        public void ZmienSileWiatru( int predkoscWiatru)
        {
            this.predkoscWiatru = predkoscWiatru;
        }
        public void ZmienKierunekWiatru(Kierunek kierunekWiatru)
        {
            this.kierunekWiatru = kierunekWiatru;
        }
        public int DajPredkoscWiatru()
        {
            return predkoscWiatru;
        }
        public Kierunek DajKierunekWiatru()
        {
            return kierunekWiatru;
        }

    }
}
