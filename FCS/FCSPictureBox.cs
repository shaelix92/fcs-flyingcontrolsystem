﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlyCoordinationSystem.Model;

namespace FlyCoordinationSystem
{
    public partial class FCSPictureBox : System.Windows.Forms.UserControl
    {
        public FCSPictureBox()
        {
            InitializeComponent();
        }
        Graphics cs;
        private Samolot samolot;
        public FCSPictureBox(Samolot st)
        {
            samolot = st;

            InitializeComponent();
           

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {       
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pictureBox1, "Nazwa samolotu: " + samolot.nazwa + "\n Kierunek samolotu: " + samolot.kierunekSamolotu);
        }
    }
}
